require_relative "sorts"

entries = %w(64 128 256 512 1024 2048 4096 8192 16384 32768)

for value in entries do
	# bubble_time = 0
	# select_time = 0
	# insert_time = 0
	# merge_time = 0
	# quick_time = 0

	# for i in (1..3)
		file = File.open("../data/#{value}.txt")

		bubble = []
		select = []
		insert = []
		merge = []
		quick = []
		for line in file.readlines do
			bubble << line.to_i
			select << line.to_i
			insert << line.to_i
			merge << line.to_i
			quick << line.to_i
		end

		t0 = Time.now

		# puts "#{print bubble}"
		bubble_sort(bubble)
		# puts "#{print bubble}"
		t1 = Time.now

		# puts "#{print select}"
		select_sort(select)
		# puts "#{print select}"
		t2 = Time.now

		# puts "#{print insert}"
		insert_sort(insert)
		# puts "#{print insert}"
		t3 = Time.now

		# puts "#{print merge}"
		merge_sort(merge)
		# puts "#{print merge}"
		t4 = Time.now

		# puts "#{print quick}"
		quick_sort(quick)
		# puts "#{print quick}"
		t5 = Time.now

		bubble_time = (t1 - t0)
		select_time = (t2 - t1)
		insert_time = (t3 - t2)
		merge_time = (t4 - t3)
		quick_time = (t5 - t4)
	# end

	puts "Quantidade de entradas:  %s" % value
	puts "Bubble sort:  %f" % (bubble_time)
	puts "Select sort:  %f" % (select_time)
	puts "Insert sort:  %f" % (insert_time)
	puts "Merge sort:   %f" % (merge_time)
	puts "Quick sort:   %f" % (quick_time)
	puts "---------------------------"
end
