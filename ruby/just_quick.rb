require_relative "sorts"

entries = %w(64 128 256 512 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576 2097152 4194304 8388608 16777216 33554432)
for value in entries do
	file = File.open("../data/#{value}.txt")
	quick = []
	for line in file.readlines do
		quick << line.to_i
	end

	t0 = Time.now

	quick_sort(quick)
	t1 = Time.now

	puts "Quantidade de entradas: %s" % value
	puts "Quick sort:   %f" % (t1 - t0)
	puts "---------------------------"
end
