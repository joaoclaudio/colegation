require_relative "sorts"

entries = %w(64 128 256 512 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576)
for value in entries do
	file = File.open("../data/#{value}.txt")
	merge = []
	quick = []
	for line in file.readlines do
		merge << line.to_i
		quick << line.to_i
	end

	t0 = Time.now

	merge_sort(merge)
	t1 = Time.now

	quick_sort(quick)
	t2 = Time.now

	puts "Quantidade de entradas:  %s" % value
	puts "Merge sort:   %f" % (t1 - t0)
	puts "Quick sort:   %f" % (t2 - t1)
	print "Tempo parcial somado:  %f", % (t2 - t0)
	puts "---------------------------"
end
