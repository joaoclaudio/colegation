def bubble_sort(array)
	for i in (0..array.length - 2) do
		for j in (0..array.length - 2 - i) do
			if (array[j] > array[j + 1])
				aux = array[j]
				array[j] = array[j + 1]
				array[j + 1] = aux
			end
		end
	end

	return array
end

def select_sort(array)
	for i in (0..array.length - 2) do
		for j in (i + 1..array.length - 1) do
			if (array[j] < array[i])
				aux = array[j]
				array[j] = array[i]
				array[i] = aux
			end
		end
	end

	return array
end

def insert_sort(array)
	for i in (1..array.length - 1) do
		aux = array[i]
		j = i - 1
		while (j >= 0 && aux < array[j])
			array[j + 1] = array[j]
			j -= 1
		end
		array[j + 1] = aux
	end

	return array
end

def merge_sort(array)
	if (array.length > 1)
		array1, array2 = partition(array)
		array1 = merge_sort(array1)
		array2 = merge_sort(array2)
		array = merge(array1, array2)
	end

	return array
end

def partition(ary)
	n = ary.length / 2
	ary1 = ary.take(n)
	ary2 = ary.drop(n)

	return ary1, ary2
end

def merge(ary1, ary2)
	ary = []

	while (ary1.length > 0 && ary2.length > 0)
		if (ary1.first < ary2.first)
			ary << ary1.shift
		else
			ary << ary2.shift
		end
	end

	while (ary1.length > 0)
		ary << ary1.shift
	end

	while (ary2.length > 0)
		ary << ary2.shift
	end

	return ary
end

def quick_sort(array)
	if (array.length > 1)

		lesser = []
		equal = []
		greater = []
		pivot = array.sample

		for n in array do
			if (n < pivot)
				lesser << n
			elsif (n > pivot)
				greater << n
			else
				equal << n
			end
		end

		lesser = quick_sort(lesser)
		greater = quick_sort(greater)
		array = lesser + equal + greater
	end

	return array
end
