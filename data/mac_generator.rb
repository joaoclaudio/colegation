File.open('64.json', 'w') do |file|
  for i in 1..64 do 
    file.print "@", rand(10...100)
  end
end

File.open('128.json', 'w') do |file|
  for i in 1..128 do
    file.print "@", rand(100...1000)
  end
end

File.open('256.json', 'w') do |file|
  for i in 1..256 do
    file.print "@", rand(100...1000)
  end
end

File.open('512.json', 'w') do |file|
  for i in 1..512 do
    file.print "@", rand(100...1000)
  end
end

File.open('1024.json', 'w') do |file|
  for i in 1..1024 do
    file.print "@", rand(1000...10000)
  end
end

File.open('2048.json', 'w') do |file|
  for i in 1..2048 do
    file.print "@", rand(1000...10000)
  end
end

File.open('4096.json', 'w') do |file|
  for i in 1..4096 do
    file.print "@", rand(1000...10000)
  end
end

File.open('8192.json', 'w') do |file|
  for i in 1..8192 do
    file.print "@", rand(1000...10000)
  end
end

File.open('16384.json','w') do |file|
  for i in 1..16384 do
    file.print "@", rand(10000...100000)
  end
end

File.open('32768.json','w') do |file|
  for i in 1..32768 do
    file.print "@", rand(10000...100000)
  end
end

File.open('65536.json','w') do |file|
  for i in 1..16384 do
    file.print "@", rand(10000...100000)
  end
end

File.open('131072.json','w') do |file|
  for i in 1..131072 do
    file.print "@", rand(100000...1000000)
  end
end

File.open('262144.json','w') do |file|
  for i in 1..262144 do
    file.print "@", rand(100000...1000000)
  end
end

File.open('524288.json','w') do |file|
  for i in 1..524288 do
    file.print "@", rand(100000...1000000)
  end
end

File.open('1048576.json','w') do |file|
  for i in 1..1048576 do
    file.print "@", rand(1000000...10000000)
  end
end

File.open('2097152.json','w') do |file|
  for i in 1..2097152 do
    file.print "@", rand(1000000...10000000)
  end
end

File.open('4194304.json','w') do |file|
  for i in 1..4194304 do
    file.print "@", rand(1000000...10000000)
  end
end

File.open('8388608.json','w') do |file|
  for i in 1..8388608 do
    file.print "@", rand(1000000...10000000)
  end
end

File.open('16777216.json','w') do |file|
  for i in 1..16777216 do
    file.print "@", rand(10000000...100000000)
  end
end

File.open('33554432.json','w') do |file|
  for i in 1..33554432 do
    file.print "@", rand(10000000...100000000)
  end
end