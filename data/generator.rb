File.open('64.txt', 'w') do |file|
  for i in 1..64 do 
    file.puts rand(10...100) 
  end
end

File.open('128.txt', 'w') do |file|
  for i in 1..128 do
    file.puts rand(100...1000)
  end
end

File.open('256.txt', 'w') do |file|
  for i in 1..256 do
    file.puts rand(100...1000)
  end
end

File.open('512.txt', 'w') do |file|
  for i in 1..512 do
    file.puts rand(100...1000)
  end
end

File.open('1024.txt', 'w') do |file|
  for i in 1..1024 do
    file.puts rand(1000...10000)
  end
end

File.open('2048.txt', 'w') do |file|
  for i in 1..2048 do
    file.puts rand(1000...10000)
  end
end

File.open('4096.txt', 'w') do |file|
  for i in 1..4096 do
    file.puts rand(1000...10000)
  end
end

File.open('8192.txt', 'w') do |file|
  for i in 1..8192 do
    file.puts rand(1000...10000)
  end
end

File.open('16384.txt','w') do |file|
  for i in 1..16384 do
    file.puts rand(10000...100000)
  end
end

File.open('32768.txt','w') do |file|
  for i in 1..32768 do
    file.puts rand(10000...100000)
  end
end

File.open('65536.txt','w') do |file|
  for i in 1..65536 do
    file.puts rand(10000...100000)
  end
end

File.open('131072.txt','w') do |file|
  for i in 1..131072 do
    file.puts rand(100000...1000000)
  end
end

File.open('262144.txt','w') do |file|
  for i in 1..262144 do
    file.puts rand(100000...1000000)
  end
end

File.open('524288.txt','w') do |file|
  for i in 1..524288 do
    file.puts rand(100000...1000000)
  end
end

File.open('1048576.txt','w') do |file|
  for i in 1..1048576 do
    file.puts rand(1000000...10000000)
  end
end

File.open('2097152.txt','w') do |file|
  for i in 1..2097152 do
    file.puts rand(1000000...10000000)
  end
end

File.open('4194304.txt','w') do |file|
  for i in 1..4194304 do
    file.puts rand(1000000...10000000)
  end
end

File.open('8388608.txt','w') do |file|
  for i in 1..8388608 do
    file.puts rand(1000000...10000000)
  end
end

File.open('16777216.txt','w') do |file|
  for i in 1..16777216 do
    file.puts rand(10000000...100000000)
  end
end

File.open('33554432.txt','w') do |file|
  for i in 1..33554432 do
    file.puts rand(10000000...100000000)
  end
end