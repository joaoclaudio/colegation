Todos os sorts - Com entradas de 64 a 32.768 elementos - Resultados em segundos
---------------------------
Quantidade de entradas:  64
Bubble sort:  0.000447
Select sort:  0.000335
Insert sort:  0.000223
Merge sort:   0.00019
Quick sort:   8.9e-05
Tempo parcial somado:  0.001284
---------------------------
Quantidade de entradas:  128
Bubble sort:  0.000927
Select sort:  0.000875
Insert sort:  0.000837
Merge sort:   0.000585
Quick sort:   0.000283
Tempo parcial somado:  0.003507
---------------------------
Quantidade de entradas:  256
Bubble sort:  0.004204
Select sort:  0.002914
Insert sort:  0.001744
Merge sort:   0.000842
Quick sort:   0.000416
Tempo parcial somado:  0.01012
---------------------------
Quantidade de entradas:  512
Bubble sort:  0.01592
Select sort:  0.011843
Insert sort:  0.007237
Merge sort:   0.001806
Quick sort:   0.000872
Tempo parcial somado:  0.037678
---------------------------
Quantidade de entradas:  1024
Bubble sort:  0.066995
Select sort:  0.054239
Insert sort:  0.028075
Merge sort:   0.003877
Quick sort:   0.002111
Tempo parcial somado:  0.155297
---------------------------
Quantidade de entradas:  2048
Bubble sort:  0.254496
Select sort:  0.196587
Insert sort:  0.115868
Merge sort:   0.008488
Quick sort:   0.004175
Tempo parcial somado:  0.579614
---------------------------
Quantidade de entradas:  4096
Bubble sort:  1.082125
Select sort:  0.794805
Insert sort:  0.470779
Merge sort:   0.019219
Quick sort:   0.008952
Tempo parcial somado:  2.37588
---------------------------
Quantidade de entradas:  8192
Bubble sort:  4.266961
Select sort:  3.42916
Insert sort:  1.95594
Merge sort:   0.045315
Quick sort:   0.018603
Tempo parcial somado:  9.715979
---------------------------
Quantidade de entradas:  16384
Bubble sort:  20.610797
Select sort:  18.193847
Insert sort:  11.9998
Merge sort:   0.179636
Quick sort:   0.072494
Tempo parcial somado:  51.056574
---------------------------
Quantidade de entradas:  32768
Bubble sort:  113.295628
Select sort:  107.340561
Insert sort:  52.744638
Merge sort:   0.412568
Quick sort:   0.116546
Tempo parcial somado:  273.909941
---------------------------
Somente os sorts recursivos - Entradas de 64 a 1.048.576 elementos - Resultados em segundos
---------------------------
Quantidade de entradas:  64
Merge sort:   0.000203999999997
Quick sort:   9.79999999799e-05
Tempo parcial somado:  0.000301999999976
---------------------------
Quantidade de entradas:  128
Merge sort:   0.00046100000003
Quick sort:   0.00022899999999
Tempo parcial somado:  0.00069000000002
---------------------------
Quantidade de entradas:  256
Merge sort:   0.000941000000012
Quick sort:   0.000841999999977
Tempo parcial somado:  0.00178299999999
---------------------------
Quantidade de entradas:  512
Merge sort:   0.00284400000004
Quick sort:   0.000981999999965
Tempo parcial somado:  0.003826
---------------------------
Quantidade de entradas:  1024
Merge sort:   0.00434300000001
Quick sort:   0.00241199999999
Tempo parcial somado:  0.006755
---------------------------
Quantidade de entradas:  2048
Merge sort:   0.012105
Quick sort:   0.00549799999999
Tempo parcial somado:  0.017603
---------------------------
Quantidade de entradas:  4096
Merge sort:   0.021652
Quick sort:   0.012143
Tempo parcial somado:  0.033795
---------------------------
Quantidade de entradas:  8192
Merge sort:   0.05423
Quick sort:   0.024188
Tempo parcial somado:  0.078418
---------------------------
Quantidade de entradas:  16384
Merge sort:   0.143668
Quick sort:   0.055018
Tempo parcial somado:  0.198686
---------------------------
Quantidade de entradas:  32768
Merge sort:   0.41001
Quick sort:   0.121273
Tempo parcial somado:  0.531283
---------------------------
Quantidade de entradas:  65536
Merge sort:   0.141679
Quick sort:   0.058462
Tempo parcial somado:  0.200141
---------------------------
Quantidade de entradas:  131072
Merge sort:   5.277366
Quick sort:   0.609544
Tempo parcial somado:  5.88691
---------------------------
Quantidade de entradas:  262144
Merge sort:   22.947135
Quick sort:   1.373611
Tempo parcial somado:  24.320746
---------------------------
Quantidade de entradas:  524288
Merge sort:   109.197248
Quick sort:   2.689714
Tempo parcial somado:  111.886962
---------------------------
Quantidade de entradas:  1048576
Merge sort:   541.27504
Quick sort:   8.78869
Tempo parcial somado:  550.06373
---------------------------
Apenas o Quick Sort - Entradas de 64 a 33.554.432 elementos - Resultados em segundos
---------------------------
Quantidade de entradas:  64
Quick sort:   0.000162000000046
---------------------------
Quantidade de entradas:  128
Quick sort:   0.000332000000071
---------------------------
Quantidade de entradas:  256
Quick sort:   0.000682000000097
---------------------------
Quantidade de entradas:  512
Quick sort:   0.00139800000011
---------------------------
Quantidade de entradas:  1024
Quick sort:   0.00336800000014
---------------------------
Quantidade de entradas:  2048
Quick sort:   0.00728699999991
---------------------------
Quantidade de entradas:  4096
Quick sort:   0.0146690000001
---------------------------
Quantidade de entradas:  8192
Quick sort:   0.0288169999999
---------------------------
Quantidade de entradas:  16384
Quick sort:   0.072938
---------------------------
Quantidade de entradas:  32768
Quick sort:   0.153628
---------------------------
Quantidade de entradas:  65536
Quick sort:   0.070731
---------------------------
Quantidade de entradas:  131072
Quick sort:   0.748511
---------------------------
Quantidade de entradas:  262144
Quick sort:   1.685932
---------------------------
Quantidade de entradas:  524288
Quick sort:   3.585146
---------------------------
Quantidade de entradas:  1048576
Quick sort:   7.791478
---------------------------
Quantidade de entradas:  2097152
Quick sort:   18.003531
---------------------------
Quantidade de entradas:  4194304
Quick sort:   30.319348
---------------------------
Quantidade de entradas:  8388608
Quick sort:   66.897707
---------------------------
Quantidade de entradas:  16777216
Quick sort:   154.733748
---------------------------
Quantidade de entradas:  33554432
Quick sort:   336.200099
---------------------------
