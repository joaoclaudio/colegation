Todos os sorts - Com entradas de 64 a 32.768 elementos - Resultados em segundos
---------------------------
Quantidade de entradas:  64
Bubble sort:  0.000432
Select sort:  0.000345
Insert sort:  0.000217
Merge sort:   0.000316
Quick sort:   0.00014
Tempo parcial somado:  0.00145
---------------------------
Quantidade de entradas:  128
Bubble sort:  0.001587
Select sort:  0.001288
Insert sort:  0.000866
Merge sort:   0.000791
Quick sort:   0.000543
Tempo parcial somado:  0.005075
---------------------------
Quantidade de entradas:  256
Bubble sort:  0.007069
Select sort:  0.004719
Insert sort:  0.002828
Merge sort:   0.001483
Quick sort:   0.000933
Tempo parcial somado:  0.017032
---------------------------
Quantidade de entradas:  512
Bubble sort:  0.026874
Select sort:  0.019838
Insert sort:  0.013013
Merge sort:   0.003501
Quick sort:   0.001654
Tempo parcial somado:  0.06488
---------------------------
Quantidade de entradas:  1024
Bubble sort:  0.106579
Select sort:  0.081977
Insert sort:  0.048893
Merge sort:   0.006283
Quick sort:   0.004533
Tempo parcial somado:  0.248265
---------------------------
Quantidade de entradas:  2048
Bubble sort:  0.427902
Select sort:  0.346538
Insert sort:  0.231024
Merge sort:   0.016206
Quick sort:   0.009651
Tempo parcial somado:  1.031321
---------------------------
Quantidade de entradas:  4096
Bubble sort:  1.960355
Select sort:  1.771699
Insert sort:  1.051928
Merge sort:   0.033722
Quick sort:   0.018521
Tempo parcial somado:  4.836225
---------------------------
Quantidade de entradas:  8192
Bubble sort:  7.99694
Select sort:  5.191923
Insert sort:  3.277935
Merge sort:   0.071412
Quick sort:   0.033783
Tempo parcial somado:  16.571993
---------------------------
Quantidade de entradas:  16384
Bubble sort:  30.510706
Select sort:  27.664446
Insert sort:  14.210929
Merge sort:   0.18484
Quick sort:   0.070975
Tempo parcial somado:  72.641896
---------------------------
Quantidade de entradas:  32768
Bubble sort:  161.142554
Select sort:  145.722048
Insert sort:  65.586714
Merge sort:   0.618084
Quick sort:   0.186285
Tempo parcial somado:  373.255685
---------------------------
Somente os sorts recursivos - Entradas de 64 a 1.048.576 elementos - Resultados em segundos
---------------------------
Quantidade de entradas:  64
Merge sort:   0.000455999999986
Quick sort:   0.000229000000047
Tempo parcial somado:  0.000685000000033
---------------------------
Quantidade de entradas:  128
Merge sort:   0.000811999999996
Quick sort:   0.000439000000028
Tempo parcial somado:  0.00125100000002
---------------------------
Quantidade de entradas:  256
Merge sort:   0.00185899999997
Quick sort:   0.000842000000034
Tempo parcial somado:  0.002701
---------------------------
Quantidade de entradas:  512
Merge sort:   0.00331900000003
Quick sort:   0.00160099999999
Tempo parcial somado:  0.00492000000003
---------------------------
Quantidade de entradas:  1024
Merge sort:   0.00773900000002
Quick sort:   0.00329900000003
Tempo parcial somado:  0.011038
---------------------------
Quantidade de entradas:  2048
Merge sort:   0.01495
Quick sort:   0.00705499999998
Tempo parcial somado:  0.022005
---------------------------
Quantidade de entradas:  4096
Merge sort:   0.031313
Quick sort:   0.014633
Tempo parcial somado:  0.045946
---------------------------
Quantidade de entradas:  8192
Merge sort:   0.0797920000001
Quick sort:   0.02968
Tempo parcial somado:  0.109472
---------------------------
Quantidade de entradas:  16384
Merge sort:   0.197906
Quick sort:   0.086842
Tempo parcial somado:  0.284748
---------------------------
Quantidade de entradas:  32768
Merge sort:   0.608413
Quick sort:   0.174649
Tempo parcial somado:  0.783062
---------------------------
Quantidade de entradas:  65536
Merge sort:   1.939472
Quick sort:   0.309222
Tempo parcial somado:  2.248694
---------------------------
Quantidade de entradas:  131072
Merge sort:   6.268391
Quick sort:   0.763169
Tempo parcial somado:  7.03156
---------------------------
Quantidade de entradas:  262144
Merge sort:   24.154337
Quick sort:   1.668477
Tempo parcial somado:  25.822814
---------------------------
Quantidade de entradas:  524288
Merge sort:   105.73905
Quick sort:   3.251618
Tempo parcial somado:  108.990668
---------------------------
Quantidade de entradas:  1048576
Merge sort:   499.376509
Quick sort:   7.751088
Tempo parcial somado:  507.127597
---------------------------
Apenas o Quick Sort - Entradas de 64 a 33.554.432 elementos - Resultados em segundos
---------------------------
Quantidade de entradas:  64
Quick sort:   0.00017200000002
---------------------------
Quantidade de entradas:  128
Quick sort:   0.000318000000107
---------------------------
Quantidade de entradas:  256
Quick sort:   0.000729000000092
---------------------------
Quantidade de entradas:  512
Quick sort:   0.00129300000003
---------------------------
Quantidade de entradas:  1024
Quick sort:   0.00524800000017
---------------------------
Quantidade de entradas:  2048
Quick sort:   0.00718899999993
---------------------------
Quantidade de entradas:  4096
Quick sort:   0.015071
---------------------------
Quantidade de entradas:  8192
Quick sort:   0.0353169999998
---------------------------
Quantidade de entradas:  16384
Quick sort:   0.077949
---------------------------
Quantidade de entradas:  32768
Quick sort:   0.16276
---------------------------
Quantidade de entradas:  65536
Quick sort:   0.334772
---------------------------
Quantidade de entradas:  131072
Quick sort:   0.768973
---------------------------
Quantidade de entradas:  262144
Quick sort:   1.596223
---------------------------
Quantidade de entradas:  524288
Quick sort:   3.273664
---------------------------
Quantidade de entradas:  1048576
Quick sort:   7.503988
---------------------------
Quantidade de entradas:  2097152
Quick sort:   16.175651
---------------------------
Quantidade de entradas:  4194304
Quick sort:   34.79005
---------------------------
Quantidade de entradas:  8388608
Quick sort:   67.0773
---------------------------
Quantidade de entradas:  16777216
Quick sort:   158.120387
---------------------------
Quantidade de entradas:  33554432
Quick sort:   353.479616
---------------------------
