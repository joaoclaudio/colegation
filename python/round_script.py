print "Todos os sorts - Com entradas de 64 a 32.768 elementos - Resultados em segundos"
print "---------------------------"
execfile('all_sorts_go.py')
print "Somente os sorts recursivos - Entradas de 64 a 1.048.576 elementos - Resultados em segundos"
print "---------------------------"
execfile('merge_vs_quick.py')
print "Apenas o Quick Sort - Entradas de 64 a 33.554.432 elementos - Resultados em segundos"
print "---------------------------"
execfile('just_quick.py')