import time
import sorts

# Opening entry files
colection = [64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576]
for value in colection:
	file = open("../data/%s.txt" % (value,), "r", 1)
	merge = []
	quick = []
	for line in file.readlines():
	    merge.append(int(line))
	    quick.append(int(line))
	file.close()

	t0 = time.clock()
	sorts.merge_sort(merge)
	t1 = time.clock()
	sorts.quick_sort(quick)
	t2 = time.clock()

	print "Quantidade de entradas: ", value
	print "Merge sort:  ", t1 - t0
	print "Quick sort:  ", t2 - t1
	print "Tempo parcial somado: ", t2 - t0
	print "---------------------------"