import time
import sorts

# Opening entry files
colection = [64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608, 16777216, 33554432]
for value in colection:
	file = open("../data/%s.txt" % (value,), "r", 1)
	quick = []
	for line in file.readlines():
	    quick.append(int(line))
	file.close()

	t0 = time.clock()
	sorts.quick_sort(quick)
	t1 = time.clock()

	print "Quantidade de entradas: ", value
	print "Quick sort:  ", t1 - t0
	print "---------------------------"