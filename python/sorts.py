import random

def bubble_sort(array):
    for i in range(0, len(array) - 1):
    	for j in range(0, len(array) - 1 - i):
    		if array[j] > array[j + 1]:
    			aux = array[j]
    			array[j] = array[j + 1]
    			array[j + 1] = aux

    return array

def select_sort(array):
	for i in range(0, len(array) - 1):
		for j in range(i + 1, len(array)):
			if array[j] < array[i]:
				aux = array[j]
				array[j] = array[i]
				array[i] = aux

	return array

def insert_sort(array):
	for i in range(1, len(array)):
		aux = array[i]
		j = i - 1
		while j >= 0 and aux < array[j]:
			array[j + 1] = array[j]
			j -= 1
		array[j + 1] = aux

	return array

def merge_sort(array):
	if len(array) > 1:
		array1, array2 = partition(array)
		array1 = merge_sort(array1)
		array2 = merge_sort(array2)
		array = merge(array1, array2)

	return array

def partition(array):
	n = len(array) / 2
	array1 = array[0:n]
	array2 = array[n::]

	return array1, array2	

def merge(a, b):
	array = []
	
	while a and b:
		if a[0] < b[0]:
			array.append(a.pop(0))
		else:
			array.append(b.pop(0))
	
	while a:
		array.append(a.pop(0))
	
	while b:
		array.append(b.pop(0))

	return array

def merge2(a, b):
	array = []
	while len(a) > 0 or len(b) > 0:
		if len(a) > 0 and len(b) > 0:
			if a[0] <= b[0]:
				array.append(a.pop(0))
			else:
				array.append(b.pop(0))
		elif len(a) > 0:
			array.append(a.pop(0))
		elif len(b) > 0:
			array.append(b.pop(0))
	return array

def quick_sort(array):
	if len(array) > 1:
		
		pivot = random.choice(array)
		lesser = []
		equal = []
		greater = []
		
		for element in array:
			if element > pivot:
				greater.append(element)
			elif element < pivot:
				lesser.append(element)
			else:
				equal.append(element)

		lesser = quick_sort(lesser)
		greater = quick_sort(greater)
		array = lesser + equal + greater

	return array