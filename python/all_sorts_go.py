import time
import sorts

# Opening entry files
colection = [64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768]
for value in colection:
	file = open("../data/%s.txt" % (value,), "r", 1)
	bubble = []
	select = []
	insert = []
	merge = []
	quick = []
	for line in file.readlines():
	    bubble.append(int(line))
	    select.append(int(line))
	    insert.append(int(line))
	    merge.append(int(line))
	    quick.append(int(line))
	file.close()

	t0 = time.clock()
	# print bubble
	sorts.bubble_sort(bubble)
	t1 = time.clock()
	# print select
	sorts.select_sort(select)
	t2 = time.clock()
	# print insert
	sorts.insert_sort(insert)
	t3 = time.clock()
	# print merge
	sorts.merge_sort(merge)
	t4 = time.clock()
	# print quick
	sorts.quick_sort(quick)
	t5 = time.clock()

	print "Quantidade de entradas: ", value
	print "Bubble sort: ", t1 - t0
	print "Select sort: ", t2 - t1
	print "Insert sort: ", t3 - t2
	print "Merge sort:  ", t4 - t3
	print "Quick sort:  ", t5 - t4
	print "---------------------------"